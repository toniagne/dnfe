---
title: API Reference

language_tabs:
- bash
- javascript

includes:

search: true

toc_footers:
- <a href='http://github.com/mpociot/documentarian'>Documentation Powered by Documentarian</a>
---
<!-- START_INFO -->
# Info

Welcome to the generated API reference.
[Get Postman Collection](http://localhost/docs/collection.json)

<!-- END_INFO -->

#general


<!-- START_505a9ce2455c5c422bbabf194b38b8dc -->
## api/v1/nfes/periodo
> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/v1/nfes/periodo" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/v1/nfes/periodo"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (500):

```json
{
    "message": "Server Error"
}
```

### HTTP Request
`GET api/v1/nfes/periodo`


<!-- END_505a9ce2455c5c422bbabf194b38b8dc -->

<!-- START_b6cf18c83ea9f050b691c5c7f6f2a39d -->
## api/v1/ultimo-lancamento/{cnpj}
> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/v1/ultimo-lancamento/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/v1/ultimo-lancamento/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (500):

```json
{
    "message": "Server Error"
}
```

### HTTP Request
`GET api/v1/ultimo-lancamento/{cnpj}`


<!-- END_b6cf18c83ea9f050b691c5c7f6f2a39d -->

<!-- START_56e5ce395a861f263ec265d787a7824b -->
## api/v1/consulta-nfe/{chnfe}
> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/v1/consulta-nfe/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/v1/consulta-nfe/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (500):

```json
{
    "message": "Server Error"
}
```

### HTTP Request
`GET api/v1/consulta-nfe/{chnfe}`


<!-- END_56e5ce395a861f263ec265d787a7824b -->

<!-- START_cee5bfdf05baf2bd363e10e20d9057fa -->
## api/v1/dfe
> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/v1/dfe" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/v1/dfe"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`GET api/v1/dfe`


<!-- END_cee5bfdf05baf2bd363e10e20d9057fa -->

<!-- START_18ed9c3cc8cf08030f61d78cba91b4a4 -->
## api/v1/dfe/{dfe}
> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/v1/dfe/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/v1/dfe/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (500):

```json
{
    "message": "Server Error"
}
```

### HTTP Request
`GET api/v1/dfe/{dfe}`


<!-- END_18ed9c3cc8cf08030f61d78cba91b4a4 -->


