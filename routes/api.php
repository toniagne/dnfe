<?php

Route::group(['prefix'=>'v1'], function(){

    // EXIBE O ÚLTIMO LANÇAMENTO PARA CONSULTA
    Route::get('/ultimo-lancamento/{cnpj}/', 'API\V1\DocumentsController@lastNumber');

    // ATUALIZA NFES (90 DIAS)
    Route::get('/atualiza-nfes/{company}/', 'API\V1\DocumentsController@loop')->name('loop');

    // EXIBE OS DETALHES DA NFE
    Route::get('/consulta-nfe/{chnfe}/', 'API\V1\DocumentsController@consult');

    // ROTINA PARA CONSULTA E GRAVAÇÃO
    Route::resource('dfe', 'API\V1\DocumentsController', ['except'=> 'create', 'edit']);
});

