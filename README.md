#  API PARA BUSCA DE DF-e

Projeto para observação de eventos de documentos fiscais

# DETALHES DO PROJETO

O projeto consiste em uma API para publicação de NF-es e DF-es para observação de eventos, gravados no banco de dados.
## MÉTODOS E TECNOLOGIAS UTILIZADAS

- [Linguagem PHP](https://www.php.net/)
- [Banco de dados MYSql](https://www.mysql.com/)
- [Framework Laravel 6](https://laravel.com/) 

### INSTALAÇÃO
Clonagem do diretório:
```
git clone https://toniagne@bitbucket.org/toniagne/dnfe.git
```

Baixe as dependências do projeto via composer. 
```
composer update
```
Configure o autoload
```
composer dump-autoload
```
Baixar as dependências (na raiz do repositório):
```
Atualizar a estrutura do banco de dados  (na raiz do repositório):
```
php artisan migrate
```
Popular o banco de dados com dados para criar usuário de exemplo (login -> exemple@dev.com.br pass-> 123123 ) :
```
php artisan migrate --seed
```

# DESENVOLVEDOR

- Toni Reniê Schott Agne <toniagne@gmail.com>