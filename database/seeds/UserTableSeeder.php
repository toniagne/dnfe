<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name'                => 'R.F ATACADO DE ALIMENTOS S/A',
            'email'               => 'teste@testedev.com.br',
            'cnpj'                => '18922507000168',
            'siglaUF'             => 'BA',
            'schemes'             => 'PL_009_V4',
            'versao'              => '4.00',
            'ultNSU'              => 226153,
            'certificate'         => 'cert.pfx',
            'certificate_pass'    => '@Senha123',
            'password'            => bcrypt('password'),
        ]);    }
}
