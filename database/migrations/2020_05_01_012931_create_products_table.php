<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('chNFe');
            $table->string('cProd')->nullable();
            $table->string('cEAN')->nullable();
            $table->string('xProd')->nullable();
            $table->string('NCM')->nullable();
            $table->string('CEST')->nullable();
            $table->string('indEscala')->nullable();
            $table->string('CFOP')->nullable();
            $table->string('uCom')->nullable();
            $table->string('qCom')->nullable();
            $table->string('vUnCom')->nullable();
            $table->string('vProd')->nullable();
            $table->string('cEANTrib')->nullable();
            $table->string('uTrib')->nullable();
            $table->string('qTrib')->nullable();
            $table->string('vUnTrib')->nullable();
            $table->string('vOutro')->nullable();
            $table->string('indTot')->nullable();
            $table->string('xPed')->nullable();
            $table->string('nItemPed')->nullable();
            $table->string('infAdProd')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
