<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNfesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nfes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('chNFe');
            $table->bigInteger('infProt_tpAmb')->nullable();
            $table->string('infProt_verAplic', 50)->nullable();
            $table->dateTime('infProt_dhRecbto')->nullable();
            $table->bigInteger('infProt_nProt')->nullable();
            $table->string('infProt_digVal', 50)->nullable();
            $table->bigInteger('infProt_cStat')->nullable();
            $table->string('infProt_xMotivo', 50)->nullable();
            $table->bigInteger('cUF')->nullable();
            $table->bigInteger('cNF')->nullable();
            $table->text('natOp')->nullable();
            $table->bigInteger('mod')->nullable();
            $table->bigInteger('serie')->nullable();
            $table->string('nNF', 20)->nullable();
            $table->dateTime('dhEmi')->nullable();
            $table->bigInteger('idDest')->nullable();
            $table->bigInteger('cMunFG')->nullable();
            $table->bigInteger('tpImp')->nullable();
            $table->bigInteger('tpEmis')->nullable();
            $table->bigInteger('cDV')->nullable();
            $table->bigInteger('tpAmb')->nullable();
            $table->bigInteger('finNFe')->nullable();
            $table->bigInteger('indFinal')->nullable();
            $table->bigInteger('indPres')->nullable();
            $table->bigInteger('procEmi')->nullable();
            $table->string('verProc', 20)->nullable();
            $table->bigInteger('emit_CNPJ')->nullable();
            $table->string('emit_xNome', 200)->nullable();
            $table->string('emit_xFant', 200)->nullable();
            $table->bigInteger('emit_IE')->nullable();
            $table->bigInteger('emit_CRT')->nullable();
            $table->string('emit_enderEmit_xLgr', 200)->nullable();
            $table->string('emit_enderEmit_nro', 50)->nullable();
            $table->string('emit_enderEmit_xCpl', 50)->nullable();
            $table->string('emit_enderEmit_xBairro', 50)->nullable();
            $table->bigInteger('emit_enderEmit_cMun')->nullable();
            $table->string('emit_enderEmit_xMun', 50)->nullable();
            $table->string('emit_enderEmit_UF', 2)->nullable();
            $table->bigInteger('emit_enderEmit_CEP')->nullable();
            $table->bigInteger('emit_enderEmit_cPais')->nullable();
            $table->string('emit_enderEmit_xPais', 50)->nullable();
            $table->bigInteger('emit_enderEmit_fone')->nullable();
            $table->bigInteger('dest_CNPJ')->nullable();
            $table->string('dest_xNome', 200)->nullable();
            $table->bigInteger('dest_indIEDest')->nullable();
            $table->bigInteger('dest_IE')->nullable();
            $table->string('dest_enderDest_xLgr', 200)->nullable();
            $table->string('dest_enderDest_nro', 50)->nullable();
            $table->string('dest_enderDest_xCpl', 50)->nullable();
            $table->string('dest_enderDest_xBairro', 50)->nullable();
            $table->bigInteger('dest_enderDest_cMun')->nullable();
            $table->string('dest_enderDest_xMun', 50)->nullable();
            $table->string('dest_enderDest_UF', 2)->nullable();
            $table->bigInteger('dest_enderDest_CEP')->nullable();
            $table->bigInteger('dest_enderDest_cPais')->nullable();
            $table->string('dest_enderDest_xPais', 50)->nullable();
            $table->double('total_ICMSTot_vBC', 10.2)->nullable();
            $table->string('total_ICMSTot_vICMS', 10.2)->nullable();
            $table->string('total_ICMSTot_vICMSDeson', 10.2)->nullable();
            $table->string('total_ICMSTot_vFCP', 10.2)->nullable();
            $table->string('total_ICMSTot_vBCST', 10.2)->nullable();
            $table->string('total_ICMSTot_vST', 10.2)->nullable();
            $table->string('total_ICMSTot_vFCPST', 10.2)->nullable();
            $table->string('total_ICMSTot_vFCPSTRet', 10.2)->nullable();
            $table->string('total_ICMSTot_vProd', 10.2)->nullable();
            $table->string('total_ICMSTot_vFrete', 10.2)->nullable();
            $table->string('total_ICMSTot_vSeg', 10.2)->nullable();
            $table->string('total_ICMSTot_vDesc', 10.2)->nullable();
            $table->string('total_ICMSTot_vII', 10.2)->nullable();
            $table->string('total_ICMSTot_vIPI', 10.2)->nullable();
            $table->string('total_ICMSTot_vIPIDevol', 10.2)->nullable();
            $table->string('total_ICMSTot_vPIS', 10.2)->nullable();
            $table->string('total_ICMSTot_vCOFINS', 10.2)->nullable();
            $table->string('total_ICMSTot_vOutro', 50)->nullable();
            $table->string('total_ICMSTot_vNF', 10.2)->nullable();
            $table->string('total_ICMSTot_vTotTrib', 10.2)->nullable();
            $table->string('transp_modFrete', 50)->nullable();
            $table->string('transp_transporta_CNPJ', 50)->nullable();
            $table->string('transp_transporta_xNome', 50)->nullable();
            $table->string('transp_transporta_IE', 50)->nullable();
            $table->text('transp_transporta_xEnder')->nullable();
            $table->string('transp_transporta_xMun', 50)->nullable();
            $table->string('transp_transporta_UF', 50)->nullable();
            $table->string('transp_vol_qVol', 50)->nullable();
            $table->string('transp_vol_esp', 50)->nullable();
            $table->string('transp_vol_pesoL', 50)->nullable();
            $table->string('transp_vol_pesoB', 50)->nullable();
            $table->string('cobr_fat_nFat', 50)->nullable();
            $table->string('cobr_fat_vOrig', 50)->nullable();
            $table->string('cobr_fat_vDesc', 50)->nullable();
            $table->string('cobr_fat_vLiq', 50)->nullable();
            $table->string('cobr_dup_nDup', 50)->nullable();
            $table->string('cobr_dup_dVenc', 50)->nullable();
            $table->string('cobr_dup_vDup', 50)->nullable();
            $table->string('pag_detPag_tPag', 50)->nullable();
            $table->string('pag_detPag_vPag', 50)->nullable();
            $table->text('infAdic_infCpl')->nullable();
            $table->string('compra_xPed', 50)->nullable();
            $table->string('infRespTec_CNPJ', 50)->nullable();
            $table->string('infRespTec_xContato', 50)->nullable();
            $table->string('infRespTec_email', 50)->nullable();
            $table->string('infRespTec_fone', 50)->nullable();



            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nfes');
    }
}
