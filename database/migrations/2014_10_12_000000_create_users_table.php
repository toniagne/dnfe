<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('cnpj')->unique();
            $table->string('siglaUF');
            $table->string('schemes');
            $table->string('versao');
            $table->string('tokenIBPT')->nullable();
            $table->string('CSC')->nullable();
            $table->string('CSCid')->nullable();
            $table->string('certificate')->unique();
            $table->string('certificate_pass')->unique();
            $table->string('password');
            $table->string('ultNSU');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
