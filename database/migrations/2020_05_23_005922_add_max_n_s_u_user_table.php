<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMaxNSUUserTable extends Migration
{

    public function up()
    {
        Schema::table('users', function(Blueprint $table) {
            $table->string('maxNSU', 255)->after('ultNSU');
        });
    }


    public function down()
    {
        Schema::table('users', function(Blueprint $table){
            $table->dropColumn('maxNSU');
        });
    }
}
