<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('cOrgao')->nullable();
            $table->string('CNPJ')->nullable();
            $table->string('chNFe')->nullable();
            $table->dateTime('dhEvento')->nullable();
            $table->string('tpEvento')->nullable();
            $table->string('nSeqEvento')->nullable();
            $table->string('xEvento')->nullable();
            $table->dateTime('dhRecbto')->nullable();
            $table->string('nProt')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}
