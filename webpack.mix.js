const mix = require('laravel-mix');


// or use require, if you prefer
const SwaggerUI = require('swagger-ui')



mix.js('resources/js/app.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css');
