<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use App\Models\Nfe;
use Illuminate\Http\Request;
use NFePHP\Common\Certificate;
use NFePHP\NFe\Tools;
use App\Models\Company;

class DocumentsController extends Controller
{
    protected $nfe;

    // CONSTRUTOR QUE INSTACIA A NFE PARA USO PÚBLICO
    public function __construct()
    {
        $this->nfe = new Nfe();
    }

    // EXIBIÇÃO DE UM RELATÓRIO POR PEŔIODO
    public function entry($start, $end){
        $document = $this->nfe->whereBetween('dhEmi', [$start, $end])->get();
    }

    // ROTINA PARA BUSCAR O ÚLTIMO NÚMERO DA SEQUENCIA DE BUSCA
    public function lastNumber($cnpj)
    {
        // INSTACIA A MODEL COMPANY
        $company = new Company($cnpj);

        // RETORNO DA RESPOSTA
         return response()->json(
             [
                 'response'=> 'succeess',
                 'ultNSU'=>$company->ConsultaNFDest($company->args()['ultNSU'],50)['ultNSU'],
                 'maxNSU'=>$company->ConsultaNFDest($company->args()['ultNSU'],50)['maxNSU']
             ]);
    }

    // CONSULTA UMA SITUAÇÃO DE NF-E PELA CHAVE
    public function consult($chnfe)
    {
        // REALIZA A BUSCA NO MODEL
        $document = $this->nfe->where('chnfe', $chnfe)->first();

        // RETORNO EM JSON
        return response()->json(
            [
                'response'=> 'succeess',
                'NFe'=>$document,
                'produtos'=>$document->products()->get()->toArray(),
                'eventos' => $document->events()->get()->toArray(),
            ]);
    }

    // ROTINA DE OPERAÇÃO PARA BUSCA DOS DOCUMENTOS FISCAIS DISPARADOS
    public function show($cnpj)
    {
        //ESTACINA OS DADOS DO CLIENTE PELO CPNJ
        $company = new Company($cnpj);

        //LISTA EM ARRAY DOS DOCUMENTOS FISCAIS
        $countNFes = 0;
        $countEvents = 0;

        // REPETIÇÃO DOS DOCUMENTOS
        foreach ($company->ConsultaNFDest($company->args()['ultNSU'],50)['listNFDes'] as $nfdes){

            // SE FOR NFE -> CADASTRA NA TABELA NFE
            if (array_key_exists("NFe", $nfdes)) {
                if ($company->checkchNFe($nfdes['protNFe']['infProt']['chNFe'])){ $countNFes++; }
                $company->setNfe($nfdes);
            }

            // SE FOR EVENTO
            elseif (array_key_exists("evento", $nfdes)){
                $countEvents++;
                $company->setEventsComplete($nfdes);
            }

        }

        // MARCA A PRÓXIMA CONSULTA
        $company->setNextConsult();

        //RETORNO EM JSON
        return response()->json(['response' => 'success', 'totalNfes' => $countNFes, 'totalEvents' => $countEvents]);
    }

    public function loop(Company $company){


        $getNSU = $this->nfe->setMaxNSU($company);
        $countItens = [];
        $ultNSU = $company->ultNSU;
        $maxNSU = $getNSU['maxNSU'];
        $loopLimit = 50;
        $iCount = 0;

        while ($ultNSU <= $maxNSU) {
            $iCount++;
            if ($iCount >= $loopLimit) {
                break;
            }

            foreach ($this->nfe->getMdfe($company, $ultNSU++) as $nfeStore) {
                if ($this->nfe->setNfe($nfeStore)) {
                    $countItens[] = 1;
                }
            }
            sleep(2);
        }

        // ATUALIZA O ÚLTIMO NSU
        $company->update([
            'ultNSU' => $getNSU['ulstNSU'] + 50,
            'maxNSU' => $getNSU['maxNSU']
        ]);

        // TIME DE 2 SEGUNDOS E ATUALIZA O LOOP
        sleep(2);

        // RETORNA PARA O LOOP COM O ultNSU
        return redirect()->to(route('loop', $company));

    }


}
