<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;
use NFePHP\Common\Certificate;
use NFePHP\NFe\Tools;
use NFePHP\NFe\Common\Standardize;

class Company extends Model
{
    protected $table = 'users';

    protected $primaryKey = 'cnpj';

    protected $fillable = [
        'ultNSU',
        'maxNSU'
    ];

     public function __construct(array $attributes = [])
     {

     }

    public function certificate(){

        $pfx = file_get_contents(public_path('certificate/'.$this->cnpj.'.pfx'));

        return  Certificate::readPfx(
            $pfx,
            $this->certificate_pass
        );
    }

    // CONFIGURAÇÕES PARA AUTORIZAÇÃO DO PLUGIN
    public function configs(){

        return [
                "atualizacao" => date('Y-m-d H:i:s'),
                "tpAmb" => 2,
                "razaosocial" => $this->name,
                "siglaUF" => $this->siglaUF,
                "cnpj" => (string)$this->cnpj,
                "schemes" => $this->schemes, //  Nome da pasta onde estão os schemas
                "versao" => $this->versao,
                "tokenIBPT" => $this->tokenIBPT, // Necessário se desejar buscar os impostos no IBPT
                "CSC" => $this->CSC, // Necessário para TODAS as operações com NFCe
                "CSCid" => $this->CSCid, // Necessário para TODAS as operações com NFCe
                "aProxyConf" => [
                    "proxyIp" => "",
                    "proxyPort" => "",
                    "proxyUser" => "",
                    "proxyPass" => ""
                ]
        ];
    }

    // MARCA O REGISTRO DA PRÓXIMA CONSULTA
    public function setNextConsult(){
        $nextConsult = $this->ultNSU;
        return self::update(['ultNSU' => $nextConsult+1]);
    }


    // VERIFICA SE EXISTE A CHAVE NUM ARRAY
    public function arrayExist($array, $index, $long = false, $complements = null){
            return Arr::exists($array, $index) ? $array[$index] : null;
    }

    // GRAVA OS PRODUTOS RELACIONADOS À NFE
    public function setProducts($chNFe, $array){
        $product = $array+$chNFe;
        return Product::create($product);
    }

    // GRAVA OS EVENTOS RELACIONADOS À NFE
    public function setEvents($array){
        return Event::create($array);
    }

    // GRAVA OS EVENTOS RELACIONADOS À NFE
    public function setEventsComplete($array){
       $event = [
           'cOrgao' => $array['evento']['infEvento']['cOrgao'],
           'CNPJ' => $array['evento']['infEvento']['CNPJ'],
           'chNFe' => $array['evento']['infEvento']['chNFe'],
           'dhEvento' => $array['evento']['infEvento']['dhEvento'],
           'tpEvento' => $array['evento']['infEvento']['tpEvento'],
           'nSeqEvento' => $array['evento']['infEvento']['nSeqEvento'],
           'xEvento' => $array['evento']['infEvento']['detEvento']['descEvento'],
           'dhRecbto' => null,
           'nProt' => $array['retEvento']['infEvento']['nProt']
       ];

       return Event::create($event);
    }

    // VERIFICA SE EXISTE UM NFE NO BD
    public function checkchNFe($chNFe){
        $query = Nfe::where('chNFe', $chNFe)->first();


        if ($query == null)
            return true;
    }

    public function getCompanyData($cnpj){

    }

}
