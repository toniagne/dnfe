<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use NFePHP\NFe\Tools;
use Illuminate\Support\Arr;

class Nfe extends Model
{
    protected  $table = 'nfes';
    protected $fillable = [
        "chNFe",
        "infProt_tpAmb",
        "infProt_verAplic",
        "infProt_dhRecbto",
        "infProt_nProt",
        "infProt_digVal",
        "infProt_cStat",
        "infProt_xMotivo",
        "cUF",
        "cNF",
        "natOp",
        "mod",
        "serie",
        "nNFv",
        "dhEmi",
        "idDest",
        "cMunFG",
        "tpImp",
        "tpEmis",
        "cDV",
        "tpAmb",
        "finNFe",
        "indFinal",
        "indPres",
        "procEmi",
        "verProc",
        "emit_CNPJ",
        "emit_xNome",
        "emit_xFant",
        "emit_IE",
        "emit_CRT",
        "emit_enderEmit_xLgr",
        "emit_enderEmit_nro",
        "emit_enderEmit_xCpl",
        "emit_enderEmit_xBairro",
        "emit_enderEmit_cMun",
        "emit_enderEmit_xMun",
        "emit_enderEmit_UF",
        "emit_enderEmit_CEP",
        "emit_enderEmit_cPais",
        "emit_enderEmit_xPais",
        "emit_enderEmit_fone ",
        "dest_CNPJ",
        "dest_xNome",
        "dest_indIEDest",
        "dest_IE ",
        "dest_enderDest_xLgr",
        "dest_enderDest_nro",
        "dest_enderDest_xCpl",
        "dest_enderDest_xBairro",
        "dest_enderDest_cMun",
        "dest_enderDest_xMun",
        "dest_enderDest_UF",
        "dest_enderDest_CEP",
        "dest_enderDest_cPais",
        "dest_enderDest_xPais ",
        "total_ICMSTot_vBC",
        "total_ICMSTot_vICMS",
        "total_ICMSTot_vICMSDeson",
        "total_ICMSTot_vFCP",
        "total_ICMSTot_vBCST",
        "total_ICMSTot_vST",
        "total_ICMSTot_vFCPST",
        "total_ICMSTot_vFCPSTRet",
        "total_ICMSTot_vProd",
        "total_ICMSTot_vFrete",
        "total_ICMSTot_vSeg",
        "total_ICMSTot_vDesc",
        "total_ICMSTot_vII",
        "total_ICMSTot_vIPI",
        "total_ICMSTot_vIPIDevol",
        "total_ICMSTot_vPIS",
        "total_ICMSTot_vCOFINS",
        "total_ICMSTot_vOutro",
        "total_ICMSTot_vNF",
        "total_ICMSTot_vTotTrib ",
        "transp_modFrete",
        "transp_transporta_CNPJ",
        "transp_transporta_xNome",
        "transp_transporta_IE",
        "transp_transporta_xEnder",
        "transp_transporta_xMun",
        "transp_transporta_UF",
        "transp_vol_qVol",
        "transp_vol_esp",
        "transp_vol_pesoL",
        "transp_vol_pesoB ",
        "cobr_fat_nFat",
        "cobr_fat_vOrig",
        "cobr_fat_vDesc",
        "cobr_fat_vLiq ",
        "cobr_dup_nDup",
        "cobr_dup_dVenc",
        "cobr_dup_vDup",
        "pag_detPag_tPag",
        "pag_detPag_vPag ",
        "infAdic_infCpl",
        "compra_xPed",
        "infRespTec_CNPJ",
        "infRespTec_xContato",
        "infRespTec_email",
        "infRespTec_fone"
    ];


    // RELACIONAMENTO COM OS EVENTOS
    public function events(){
        return $this->hasMany(Event::class, 'chNFe', 'chNFe');
    }

    //RELACIONAMEN TO COM OS PRODUTOS
    public function products(){
        return $this->hasMany(Product::class, 'chNFe', 'chNFe');
    }

    public function setMaxNSU(Company $company){
        $mdfe = new Tools(json_encode($company->configs()), $company->certificate());
        $mdfe->model('55');
        $mdfe->setEnvironment(1);

        try {
            //executa a busca pelos documentos
            $resp = $mdfe->sefazDistDFe(0);
        } catch (\Exception $e) {
            echo $e->getMessage();
            //tratar o erro
        }

        //extrair e salvar os retornos
        $dom = new \DOMDocument();

        // VARIÁVEL $resp É O ARQUIVO XML
        $dom->loadXML($resp);
        $node = $dom->getElementsByTagName('retDistDFeInt')->item(0);
        $ultNSU = $node->getElementsByTagName('ultNSU')->item(0)->nodeValue;
        $maxNSU = $node->getElementsByTagName('maxNSU')->item(0)->nodeValue;

        return ['ulstNSU' => $ultNSU, 'maxNSU' => $maxNSU];

    }

    public function getMdfe(Company $company, $ultNSU){

        $mdfe = new Tools(json_encode($company->configs()), $company->certificate());
        $mdfe->model('55');
        $mdfe->setEnvironment(1);

        try {
            //executa a busca pelos documentos
            $resp = $mdfe->sefazDistDFe($ultNSU);
        } catch (\Exception $e) {
            echo $e->getMessage();
            //tratar o erro
        }

        //extrair e salvar os retornos
        $dom = new \DOMDocument();

        // VARIÁVEL $resp É O ARQUIVO XML
        $dom->loadXML($resp);

        $node = $dom->getElementsByTagName('retDistDFeInt')->item(0);
        $xMotivo = $node->getElementsByTagName('xMotivo')->item(0)->nodeValue;
        $ultNSU = $node->getElementsByTagName('ultNSU')->item(0)->nodeValue;
        $maxNSU = $node->getElementsByTagName('maxNSU')->item(0)->nodeValue;
        $lote = $node->getElementsByTagName('loteDistDFeInt')->item(0);

        //essas tags irão conter os documentos zipados
        if ($lote != NULL) {
            $docs = $lote->getElementsByTagName('docZip');

            foreach ($docs as $doc) {
                $numnsu = $doc->getAttribute('NSU');
                $schema = $doc->getAttribute('schema');

                //descompacta o documento e recupera o XML original
                $content[$schema][] = gzdecode(base64_decode($doc->nodeValue));
                //identifica o tipo de documento
                $tipo = substr($schema, 0, 6);
            }


            foreach ($content['procNFe_v4.00.xsd'] as $nfdes) {
                $xml = simplexml_load_string($nfdes);
                $convertJson = json_encode($xml);
                $listNfdes[] = json_decode($convertJson, TRUE);
            }


        } else { $listNfdes[] = []; }

        return $listNfdes;
    }

    public function setNfe($nfeArray){

            if (Arr::exists($nfeArray, 'protNFe')){
            $consultNfe = self::where('chNFe', $nfeArray['protNFe']['infProt']['chNFe'])->first();

            if ($consultNfe) { return false; } else {
                $dataRequest = [
                    "chNFe" => $nfeArray['protNFe']['infProt']['chNFe'],
                    "infProt_tpAmb" => $nfeArray['protNFe']['infProt']['tpAmb'],
                    "infProt_verAplic" => $nfeArray['protNFe']['infProt']['verAplic'],
                    "infProt_dhRecbto" => $nfeArray['protNFe']['infProt']['dhRecbto'],
                    "infProt_nProt" => $nfeArray['protNFe']['infProt']['nProt'],
                    "infProt_digVal" => $nfeArray['protNFe']['infProt']['digVal'],
                    "infProt_cStat" => $nfeArray['protNFe']['infProt']['cStat'],
                    "infProt_xMotivo" => $nfeArray['protNFe']['infProt']['xMotivo'],

                    //IDENTIFICAÇÃO DA NF
                    "cUF" => $nfeArray['NFe']['infNFe']['ide']['cUF'],
                    "cNF" => $nfeArray['NFe']['infNFe']['ide']['cNF'],
                    "natOp" => $nfeArray['NFe']['infNFe']['ide']['natOp'],
                    "mod" => $nfeArray['NFe']['infNFe']['ide']['mod'],
                    "serie" => $nfeArray['NFe']['infNFe']['ide']['serie'],
                    "nNF" => $nfeArray['NFe']['infNFe']['ide']['nNF'],
                    "dhEmi" => $nfeArray['NFe']['infNFe']['ide']['dhEmi'],
                    "idDest" => $nfeArray['NFe']['infNFe']['ide']['idDest'],
                    "cMunFG" => $nfeArray['NFe']['infNFe']['ide']['cMunFG'],
                    "tpImp" => $nfeArray['NFe']['infNFe']['ide']['tpImp'],
                    "tpEmis" => $nfeArray['NFe']['infNFe']['ide']['tpEmis'],
                    "cDV" => $nfeArray['NFe']['infNFe']['ide']['cDV'],
                    "tpAmb" => $nfeArray['NFe']['infNFe']['ide']['tpAmb'],
                    "finNFe" => $nfeArray['NFe']['infNFe']['ide']['finNFe'],
                    "indFinal" => $nfeArray['NFe']['infNFe']['ide']['indFinal'],
                    "indPres" => $nfeArray['NFe']['infNFe']['ide']['indPres'],
                    "procEmi" => $nfeArray['NFe']['infNFe']['ide']['procEmi'],
                    "verProc" => $nfeArray['NFe']['infNFe']['ide']['verProc'],

                    // DADOS EMITENTE DA NF
                    "emit_CNPJ" => $nfeArray['NFe']['infNFe']['emit']['CNPJ'],
                    "emit_xNome" => $nfeArray['NFe']['infNFe']['emit']['xNome'],
                    "emit_xFant" => Arr::has($nfeArray['NFe']['infNFe']['emit'], 'xFant') ? $nfeArray['NFe']['infNFe']['emit']['xFant'] : "",
                    "emit_IE" => $nfeArray['NFe']['infNFe']['emit']['IE'],
                    "emit_CRT" => $nfeArray['NFe']['infNFe']['emit']['CRT'],
                    "emit_enderEmit_xLgr" => $nfeArray['NFe']['infNFe']['emit']['enderEmit']['xLgr'],
                    "emit_enderEmit_nro" => $nfeArray['NFe']['infNFe']['emit']['enderEmit']['nro'],
                    "emit_enderEmit_xBairro" => $nfeArray['NFe']['infNFe']['emit']['enderEmit']['xBairro'],
                    "emit_enderEmit_cMun" => $nfeArray['NFe']['infNFe']['emit']['enderEmit']['cMun'],
                    "emit_enderEmit_xMun" => $nfeArray['NFe']['infNFe']['emit']['enderEmit']['xMun'],
                    "emit_enderEmit_UF" => $nfeArray['NFe']['infNFe']['emit']['enderEmit']['UF'],
                    "emit_enderEmit_CEP" => $nfeArray['NFe']['infNFe']['emit']['enderEmit']['CEP'],
                    "emit_enderEmit_cPais" => $this->arrayExist($nfeArray['NFe']['infNFe']['emit']['enderEmit'], 'cPais'),
                    "emit_enderEmit_xPais" => $this->arrayExist($nfeArray['NFe']['infNFe']['emit']['enderEmit'], ' xPais'),
                    "emit_enderEmit_fone " => $this->arrayExist($nfeArray['NFe']['infNFe']['emit']['enderEmit'], 'fone'),

                    // DADOS DESTINATÁRIO
                    "dest_CNPJ" => $nfeArray['NFe']['infNFe']['dest']['CNPJ'],
                    "dest_xNome" => $nfeArray['NFe']['infNFe']['dest']['xNome'],
                    "dest_indIEDest" => $nfeArray['NFe']['infNFe']['dest']['indIEDest'],
                    "dest_IE " => $nfeArray['NFe']['infNFe']['dest']['IE'],
                    "dest_enderDest_xLgr" => $nfeArray['NFe']['infNFe']['dest']['enderDest']['xLgr'],
                    "dest_enderDest_nro" => $nfeArray['NFe']['infNFe']['dest']['enderDest']['nro'],
                    "dest_enderDest_xCpl" => Arr::exists($nfeArray['NFe']['infNFe']['dest']['enderDest'], 'xCpl') ? $nfeArray['NFe']['infNFe']['dest']['enderDest']['xCpl'] : "",
                    "dest_enderDest_xBairro" => $nfeArray['NFe']['infNFe']['dest']['enderDest']['xBairro'],
                    "dest_enderDest_cMun" => $nfeArray['NFe']['infNFe']['dest']['enderDest']['cMun'],
                    "dest_enderDest_xMun" => $nfeArray['NFe']['infNFe']['dest']['enderDest']['xMun'],
                    "dest_enderDest_UF" => $nfeArray['NFe']['infNFe']['dest']['enderDest']['UF'],
                    "dest_enderDest_CEP" => $nfeArray['NFe']['infNFe']['dest']['enderDest']['CEP'],
                    "dest_enderDest_cPais" => $this->arrayExist($nfeArray['NFe']['infNFe']['dest']['enderDest'], 'cPais'),
                    "dest_enderDest_xPais " => $this->arrayExist($nfeArray['NFe']['infNFe']['dest']['enderDest'], 'xPais'),

                    // TOTAIS
                    "total_ICMSTot_vBC" => $nfeArray['NFe']['infNFe']['total']['ICMSTot']['vBC'],
                    "total_ICMSTot_vICMS" => $nfeArray['NFe']['infNFe']['total']['ICMSTot']['vICMS'],
                    "total_ICMSTot_vICMSDeson" => $nfeArray['NFe']['infNFe']['total']['ICMSTot']['vICMSDeson'],
                    "total_ICMSTot_vFCP" => $nfeArray['NFe']['infNFe']['total']['ICMSTot']['vFCP'],
                    "total_ICMSTot_vBCST" => $nfeArray['NFe']['infNFe']['total']['ICMSTot']['vBCST'],
                    "total_ICMSTot_vST" => $nfeArray['NFe']['infNFe']['total']['ICMSTot']['vST'],
                    "total_ICMSTot_vFCPST" => $nfeArray['NFe']['infNFe']['total']['ICMSTot']['vFCPST'],
                    "total_ICMSTot_vFCPSTRet" => $nfeArray['NFe']['infNFe']['total']['ICMSTot']['vFCPSTRet'],
                    "total_ICMSTot_vProd" => $nfeArray['NFe']['infNFe']['total']['ICMSTot']['vProd'],
                    "total_ICMSTot_vFrete" => $nfeArray['NFe']['infNFe']['total']['ICMSTot']['vFrete'],
                    "total_ICMSTot_vSeg" => $nfeArray['NFe']['infNFe']['total']['ICMSTot']['vSeg'],
                    "total_ICMSTot_vDesc" => $nfeArray['NFe']['infNFe']['total']['ICMSTot']['vDesc'],
                    "total_ICMSTot_vII" => $nfeArray['NFe']['infNFe']['total']['ICMSTot']['vII'],
                    "total_ICMSTot_vIPI" => $nfeArray['NFe']['infNFe']['total']['ICMSTot']['vIPI'],
                    "total_ICMSTot_vIPIDevol" => $nfeArray['NFe']['infNFe']['total']['ICMSTot']['vIPIDevol'],
                    "total_ICMSTot_vPIS" => $nfeArray['NFe']['infNFe']['total']['ICMSTot']['vPIS'],
                    "total_ICMSTot_vCOFINS" => $nfeArray['NFe']['infNFe']['total']['ICMSTot']['vCOFINS'],
                    "total_ICMSTot_vOutro" => $nfeArray['NFe']['infNFe']['total']['ICMSTot']['vOutro'],
                    "total_ICMSTot_vNF" => $nfeArray['NFe']['infNFe']['total']['ICMSTot']['vNF'],

                    // TRANSPORTADORA
                    "transp_modFrete" => $nfeArray['NFe']['infNFe']['transp']['modFrete'],
                    "transp_transporta_CNPJ" => Arr::exists($nfeArray['NFe']['infNFe']['transp'], 'transporta') ? $this->arrayExist($nfeArray['NFe']['infNFe']['transp']['transporta'], 'CPF') : "",
                    "transp_transporta_xNome" => Arr::exists($nfeArray['NFe']['infNFe']['transp'], 'transporta') ? $this->arrayExist($nfeArray['NFe']['infNFe']['transp']['transporta'], 'xNome') : "",
                    "transp_transporta_IE" => Arr::exists($nfeArray['NFe']['infNFe']['transp'], 'transporta') ? $this->arrayExist($nfeArray['NFe']['infNFe']['transp']['transporta'], 'IE') : "",
                    "transp_transporta_xEnder" => Arr::exists($nfeArray['NFe']['infNFe']['transp'], 'transporta') ? $this->arrayExist($nfeArray['NFe']['infNFe']['transp']['transporta'], 'xEnder') : "",
                    "transp_transporta_xMun" => Arr::exists($nfeArray['NFe']['infNFe']['transp'], 'transporta') ? $this->arrayExist($nfeArray['NFe']['infNFe']['transp']['transporta'], 'xMun') : "",
                    "transp_transporta_UF" => Arr::exists($nfeArray['NFe']['infNFe']['transp'], 'transporta') ? $this->arrayExist($nfeArray['NFe']['infNFe']['transp']['transporta'], 'UF') : "",

                    // TRANSPORTADORA (VOLUME)
                    "transp_vol_qVol"       => Arr::has($nfeArray['NFe']['infNFe']['transp'], 'vol') ? $this->arrayExist($nfeArray['NFe']['infNFe']['transp']['vol'], 'qVol') : "",
                    "transp_vol_esp"        => Arr::has($nfeArray['NFe']['infNFe']['transp'], 'vol') ? $this->arrayExist($nfeArray['NFe']['infNFe']['transp']['vol'], 'esp') : "",
                    "transp_vol_pesoL"      => Arr::has($nfeArray['NFe']['infNFe']['transp'], 'vol') ? $this->arrayExist($nfeArray['NFe']['infNFe']['transp']['vol'], 'pesoL') : "",
                    "transp_vol_pesoB "     => Arr::has($nfeArray['NFe']['infNFe']['transp'], 'vol') ? $this->arrayExist($nfeArray['NFe']['infNFe']['transp']['vol'], 'pesoB') : "",

                    // COBRANÇA (FATURAMENTO)
                    "cobr_fat_nFat" => Arr::exists($nfeArray['NFe']['infNFe'], 'cobr') ? $nfeArray['NFe']['infNFe']['cobr']['fat']['nFat'] : "",
                    "cobr_fat_vOrig" => Arr::exists($nfeArray['NFe']['infNFe'], 'cobr') ? $nfeArray['NFe']['infNFe']['cobr']['fat']['vOrig'] : "",
                    "cobr_fat_vDesc" => Arr::exists($nfeArray['NFe']['infNFe'], 'cobr') ? $nfeArray['NFe']['infNFe']['cobr']['fat']['vDesc'] : "",
                    "cobr_fat_vLiq " => Arr::exists($nfeArray['NFe']['infNFe'], 'cobr') ? $nfeArray['NFe']['infNFe']['cobr']['fat']['vLiq'] : "",

                    // COBRANÇA (DUPLICATA)
                    "cobr_dup_nDup" => Arr::exists($nfeArray['NFe']['infNFe'], 'cobr') ? Arr::exists($nfeArray['NFe']['infNFe']['cobr'], 'dup') ? $this->arrayExist($nfeArray['NFe']['infNFe']['cobr']['dup'], 'nDup') : "" : "",
                    "cobr_dup_dVenc" => Arr::exists($nfeArray['NFe']['infNFe'], 'cobr') ? Arr::exists($nfeArray['NFe']['infNFe']['cobr'], 'dup') ? $this->arrayExist($nfeArray['NFe']['infNFe']['cobr']['dup'], 'dVenc') : "" : "",
                    "cobr_dup_vDup" => Arr::exists($nfeArray['NFe']['infNFe'], 'cobr') ? Arr::exists($nfeArray['NFe']['infNFe']['cobr'], 'dup') ? $this->arrayExist($nfeArray['NFe']['infNFe']['cobr']['dup'], 'vDup') : "" : "",


                    "pag_detPag_tPag" => $nfeArray['NFe']['infNFe']['pag']['detPag']['tPag'],
                    "pag_detPag_vPag " => $nfeArray['NFe']['infNFe']['pag']['detPag']['vPag'],

                    "infAdic_infCpl" => $this->arrayExist($nfeArray['NFe']['infNFe']['infAdic'], 'infCpl'),

                    "compra_xPed" => Arr::exists($nfeArray['NFe']['infNFe'], 'compra') ? $nfeArray['NFe']['infNFe']['compra']['xPed'] : "",

                    "infRespTec_CNPJ" => Arr::exists($nfeArray['NFe']['infNFe'], 'infRespTec') ? $nfeArray['NFe']['infNFe']['infRespTec']['CNPJ'] : "",
                    "infRespTec_xContato" => Arr::exists($nfeArray['NFe']['infNFe'], 'infRespTec') ? $nfeArray['NFe']['infNFe']['infRespTec']['xContato'] : "",
                    "infRespTec_email" => Arr::exists($nfeArray['NFe']['infNFe'], 'infRespTec') ? $nfeArray['NFe']['infNFe']['infRespTec']['email'] : "",
                    "infRespTec_fone" => Arr::exists($nfeArray['NFe']['infNFe'], 'infRespTec') ? $nfeArray['NFe']['infNFe']['infRespTec']['fone'] : "",
                ];


                $this->setProducts(['chNFe' => $nfeArray['protNFe']['infProt']['chNFe']], $nfeArray['NFe']['infNFe']['det']);

                return self::create($dataRequest);
            }
            }
    }


    // VERIFICA SE EXISTE A CHAVE NUM ARRAY
    public function arrayExist($array, $index, $long = false, $complements = null){
        return Arr::exists($array, $index) ? $array[$index] : null;
    }

    // GRAVA OS PRODUTOS RELACIONADOS À NFE
    public function setProducts($chNFe, $products){
        if (Arr::exists($products, 'prod')) {
            $product = $products['prod']+$chNFe;
            return Product::create($product);
        } else {
            foreach ($products as $array){

                if (Arr::exists($array, 'prod')){
                    $product = $array['prod']+$chNFe;
                    return Product::create($product);
                }  else {}

            }
        }



    }
}
