<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
        'chNFe',
        'cProd',
        'cEAN',
        'xProd',
        'NCM',
        'CEST',
        'indEscala',
        'CFOP',
        'uCom',
        'qCom',
        'vUnCom',
        'vProd',
        'cEANTrib',
        'uTrib',
        'qTrib',
        'vUnTrib',
        'vOutro',
        'indTot',
        'xPed',
        'nItemPed'
    ];
}
