<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
   protected $fillable = [
       'cOrgao',
       'CNPJ',
       'chNFe',
       'dhEvento',
       'tpEvento',
       'nSeqEvento',
       'xEvento',
       'dhRecbto',
       'nProt'
       ];
}
